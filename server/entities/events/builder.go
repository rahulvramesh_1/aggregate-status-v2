package es

import (
	"time"

	jsoniter "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
	"github.com/golang/protobuf/ptypes"
)

func CopyEvent(e Event) Event {
	return Event{
		ID:               e.ID,
		ProcessID:        e.ProcessID,
		ProcessCreatedAt: e.ProcessCreatedAt,
		SubProcessID:     e.SubProcessID,
		EventSourceID:    e.EventSourceID,
		EventSourceName:  e.EventSourceName,
		Version:          e.Version,
		Sequence:         e.Sequence,
		Type:             e.Type,
		Data:             string(e.Data),
		CreatedAt:        e.CreatedAt,
	}
}

func BuildEventsFromStatus(process *pb.Process, prev *pb.Status, curr *pb.Status) ([]Event, error) {
	var events []Event
	var data []byte

	procTS, err := ptypes.Timestamp(process.GetTimestamp())
	if err != nil {
		return events, err
	}

	event := Event{
		ID:               0,
		ProcessID:        process.GetId(),
		ProcessCreatedAt: procTS,
		SubProcessID:     process.GetSubProcess().GetId(),
		EventSourceID:    prev.GetId(),
		EventSourceName:  ENTITY_NAME,
		Version:          prev.GetVersion(),
		Sequence:         0,
		Type:             "",
		Data:             string(data),
		CreatedAt:        time.Now(),
	}

	if prev.Version > 0 && prev.Id > 0 && process.SubProcess.Name == pb.SubProcessName_UPDATE_STATUS {
		if prev.Name != curr.Name {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = NAME_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}

		if prev.Description != curr.Description {
			data, _ = jsoniter.Marshal(&curr)
			event.Type = DESCRIPTION_CHANGED
			event.Data = string(data)
			event.Sequence++
			events = append(events, CopyEvent(event))
		}
	} else {
		data, _ = jsoniter.Marshal(&curr)
		event.Type = STATUS_ADDED
		event.Data = string(data)
		event.Sequence++
		events = append(events, CopyEvent(event))
	}
	return events, err
}

func BuildStatusFromEvents(es EventSource, events []Event, ss *pb.Status) (*pb.Status, error) {
	var (
		err    error
		status *pb.Status
		temp   *pb.Status
	)

	//if snapshot exists
	if ss.Id > 0 {
		status = ss
	}

	for _, event := range events {
		jsoniter.Unmarshal([]byte(event.Data), &temp)
		switch eventType := event.Type; eventType {
		case STATUS_ADDED:
			status = temp
		case NAME_CHANGED:
			status.Name = temp.Name
		case DESCRIPTION_CHANGED:
			status.Description = temp.Description
		}
	}

	//compensate payload
	status.Id = es.ID
	status.Version = es.Version
	status.CreatedAt, _ = ptypes.TimestampProto(es.CreatedAt)
	status.UpdatedAt, _ = ptypes.TimestampProto(es.UpdatedAt)

	return status, err
}
