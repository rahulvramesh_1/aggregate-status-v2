package eventstore

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
)

func Init(db *sql.DB) {
	var (
		err error
	)

	_, err = db.Exec(`
		-- create partition function
		CREATE OR REPLACE FUNCTION index_partition(table_name text, partition_name text) RETURNS VOID AS
		$BODY$
		DECLARE
		index_columns TEXT;
		BEGIN
			IF table_name = 'e' THEN
			 index_columns := '(proc_id, sproc_id, timezone(''UTC''::text, proc_ts), timezone(''UTC''::text, sproc_ts))';
			ELSIF table_name = 'proc' THEN
			 index_columns := '(id, timezone(''UTC''::text, ts))';
			ELSE
			 index_columns := '(id, proc_id, timezone(''UTC''::text, ts))';
			END IF;
		  -- Ensure we have all the necessary indices in this partition;
		  EXECUTE 'CREATE INDEX IF NOT EXISTS ' || partition_name || '_idx ON ' || partition_name || ' ' || index_columns;
		END;
		$BODY$
		LANGUAGE plpgsql;

		-- create delete parent row function
		CREATE OR REPLACE FUNCTION delete_parent_row(table_parent text, id bigint) RETURNS VOID AS
		$BODY$
		DECLARE
		BEGIN
		  EXECUTE 'DELETE FROM ' || table_parent || ' WHERE id = ' || id;
		END;
		$BODY$
		LANGUAGE plpgsql;

		-- create trigger function
		CREATE OR REPLACE FUNCTION partition_func() RETURNS trigger AS
		$BODY$
		DECLARE
		partition_date_format TIMESTAMP;
		partition_date TEXT;
		partition TEXT;
		BEGIN
		IF TG_TABLE_NAME = 'e' THEN
				partition_date_format := date_trunc('month', NEW.proc_ts AT TIME ZONE 'UTC');
				partition_date := to_char(partition_date_format, 'YYYYMM');
				partition := TG_TABLE_NAME || '_proc_ts_' || partition_date;
		ELSE
				partition_date_format := date_trunc('month', NEW.ts AT TIME ZONE 'UTC');
				partition_date := to_char(partition_date_format,'YYYYMM');
				partition := TG_TABLE_NAME || '_ts_' || partition_date;
		END IF;

		-- create partition if not exists
		RAISE NOTICE 'A partition will be created %',partition;
		EXECUTE 'CREATE TABLE IF NOT EXISTS ' || partition || ' () INHERITS (' || TG_TABLE_NAME || ');';
		PERFORM index_partition(TG_TABLE_NAME, partition);

		-- insert data to new partition
		EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || TG_RELNAME || ' ' || quote_literal(NEW) || ').* RETURNING id;';

		-- if we would like to have return values ( use ORM )
			-- 1. RETURN NEW instead of NULL
			-- 2. change from 'BEFORE' to 'AFTER' INSERT in trigger section
			-- 3. PERFORM delete_parent_row(TG_TABLE_NAME, NEW.id)
		-- if not no need to execute the insert to the parent table by returning NULL
		RETURN NULL;

		END;
		$BODY$
		LANGUAGE plpgsql VOLATILE
		COST 100;
		`)

	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
    CREATE TABLE IF NOT EXISTS es (
      id SERIAL PRIMARY KEY NOT NULL,
      name VARCHAR(32) NOT NULL,
      ver INT NOT NULL,
			ts TIMESTAMP WITH TIME ZONE NOT NULL,
			ts_m TIMESTAMP WITH TIME ZONE
    );

		CREATE INDEX IF NOT EXISTS es_idx ON es (timezone('UTC'::text, ts), name, id);

		`)
	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS e (
			id BIGSERIAL PRIMARY KEY NOT NULL,
			proc_id UUID NOT NULL,
			proc_ts TIMESTAMP WITH TIME ZONE NOT NULL,
			sproc_id UUID NOT NULL,
			sproc_ts TIMESTAMP WITH TIME ZONE NOT NULL,
			es_id BIGINT NOT NULL,
			es_name VARCHAR(32) NOT NULL,
			ver INT,
			seq INT,
			type VARCHAR(32),
			data JSON NOT NULL
		);

		DROP TRIGGER IF EXISTS e_partition_trigger on e;

		CREATE TRIGGER e_partition_trigger
		BEFORE INSERT ON e
		FOR EACH ROW EXECUTE PROCEDURE partition_func();

		CREATE OR REPLACE VIEW e_partitions AS
		SELECT nmsp_parent.nspname AS parent_schema,
		parent.relname AS parent,
		nmsp_child.nspname AS child_schema,
		child.relname AS child
		FROM pg_inherits
		JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
		JOIN pg_class child ON pg_inherits.inhrelid = child.oid
		JOIN pg_namespace nmsp_parent ON nmsp_parent.oid = parent.relnamespace
		JOIN pg_namespace nmsp_child ON nmsp_child.oid = child.relnamespace
		WHERE parent.relname='e' ;
    `)

	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS proc (
			id UUID PRIMARY KEY NOT NULL,
			name VARCHAR(32) NOT NULL,
			orig VARCHAR(32) NOT NULL,
			ts TIMESTAMP WITH TIME ZONE NOT NULL,
			user_id BIGINT NOT NULL
		);

		CREATE INDEX IF NOT EXISTS proc_idx ON proc (timezone('UTC'::text, ts), name);

		DROP TRIGGER IF EXISTS proc_partition_trigger on proc;

		CREATE TRIGGER proc_partition_trigger
		BEFORE INSERT ON proc
		FOR EACH ROW EXECUTE PROCEDURE partition_func();

		CREATE OR REPLACE VIEW proc_partitions AS
		SELECT nmsp_parent.nspname AS parent_schema,
		parent.relname AS parent,
		nmsp_child.nspname AS child_schema,
		child.relname AS child
		FROM pg_inherits
		JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
		JOIN pg_class child ON pg_inherits.inhrelid = child.oid
		JOIN pg_namespace nmsp_parent ON nmsp_parent.oid = parent.relnamespace
		JOIN pg_namespace nmsp_child ON nmsp_child.oid = child.relnamespace
		WHERE parent.relname='proc' ;
`)

	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS sproc (
			id UUID PRIMARY KEY NOT NULL,
			proc_id UUID NOT NULL,
			name VARCHAR(32) NOT NULL,
			orig VARCHAR(32) NOT NULL,
			ts TIMESTAMP WITH TIME ZONE NOT NULL
		);

		CREATE INDEX IF NOT EXISTS sproc_idx ON sproc (timezone('UTC'::text, ts), proc_id, name);

		DROP TRIGGER IF EXISTS sproc_partition_trigger on sproc;

		CREATE TRIGGER sproc_partition_trigger
		BEFORE INSERT ON sproc
		FOR EACH ROW EXECUTE PROCEDURE partition_func();

		CREATE OR REPLACE VIEW sproc_partitions AS
		SELECT nmsp_parent.nspname AS parent_schema,
		parent.relname AS parent,
		nmsp_child.nspname AS child_schema,
		child.relname AS child
		FROM pg_inherits
		JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
		JOIN pg_class child ON pg_inherits.inhrelid = child.oid
		JOIN pg_namespace nmsp_parent ON nmsp_parent.oid = parent.relnamespace
		JOIN pg_namespace nmsp_child ON nmsp_child.oid = child.relnamespace
		WHERE parent.relname='sproc';
		`)

	if err != nil {
		log.Error(err)
	}

	_, err = db.Exec(`
	CREATE TABLE IF NOT EXISTS ss (
		es_name VARCHAR(32) NOT NULL,
		es_id BIGINT NOT NULL,
		ver INT NOT NULL,
		data JSON NOT NULL,
		ts TIMESTAMP WITH TIME ZONE NOT NULL
	);
	`)

	if err != nil {
		log.Error(err)
	}
}
