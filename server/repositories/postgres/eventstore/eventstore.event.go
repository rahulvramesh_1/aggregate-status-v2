package eventstore

import (
	"database/sql"
	"encoding/json"
	"errors"
	"time"

	log "github.com/sirupsen/logrus"

	es "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
	"bitbucket.org/evhivetech/aggregate-status-v2/server/repositories"
)

type C_DB struct {
	db *sql.DB
}

func GetCommandClientDB(db *sql.DB) repository.C {
	return &C_DB{db: db}
}

func (c *C_DB) WriteNewStream(process *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error) {
	var (
		err  error
		tx   *sql.Tx
		stmt *sql.Stmt
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return process, eventSource, eventStreams, err
	}
	//add process
	{
		procExists := 0
		if err = tx.QueryRow("SELECT COUNT(*) FROM proc WHERE id = $1", process.Id).
			Scan(&procExists); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if procExists < 1 {
			stmt, err = tx.Prepare("INSERT INTO proc VALUES ($1, $2, $3, $4, $5);")
			if err != nil {
				log.Error(err)
				return process, eventSource, eventStreams, err
			}

			if _, err = stmt.Exec(
				process.Id,
				process.Name,
				process.Origin,
				eventSource.CreatedAt,
				process.UserId); err != nil {
				log.Error(err)
				tx.Rollback()
				return process, eventSource, eventStreams, err
			}
			defer stmt.Close()
		}
	}
	//record subsprocess
	{
		stmt, err = tx.Prepare("INSERT INTO sproc VALUES ($1, $2, $3, $4, $5);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		if _, err = stmt.Exec(
			process.SubProcess.Id,
			process.Id,
			process.SubProcess.Name,
			process.Origin,
			eventSource.CreatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO es VALUES (DEFAULT, $1, $2, $3, $4) RETURNING *;")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if err = stmt.QueryRow(eventSource.Name, eventSource.Version+1, eventSource.CreatedAt, time.Time{}).
			Scan(
				&eventSource.ID,
				&eventSource.Name,
				&eventSource.Version,
				&eventSource.CreatedAt,
				&eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO e VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		for _, e := range eventStreams {
			if _, err = stmt.Exec(
				process.Id,
				eventSource.CreatedAt,
				process.SubProcess.Id,
				eventSource.CreatedAt,
				eventSource.ID,
				eventSource.Name,
				eventSource.Version,
				e.Sequence,
				e.Type,
				e.Data); err != nil {
				tx.Rollback()
				log.Error(err)
				return process, eventSource, eventStreams, err
			}
		}

		defer stmt.Close()
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return process, eventSource, eventStreams, err
	}

	return process, eventSource, eventStreams, err
}

func (c *C_DB) AppendToStream(process *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error) {
	var (
		err         error
		tx          *sql.Tx
		stmt        *sql.Stmt
		lastVersion int64
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return process, eventSource, eventStreams, err
	}
	//add process
	{
		procExists := 0
		if err = tx.QueryRow("SELECT COUNT(*) FROM proc WHERE id = $1", process.Id).
			Scan(&procExists); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if procExists < 1 {
			stmt, err = tx.Prepare("INSERT INTO proc VALUES ($1, $2, $3, $4, $5);")
			if err != nil {
				log.Error(err)
				return process, eventSource, eventStreams, err
			}

			if _, err = stmt.Exec(
				process.Id,
				process.Name,
				process.Origin,
				eventSource.UpdatedAt,
				process.UserId); err != nil {
				log.Error(err)
				tx.Rollback()
				return process, eventSource, eventStreams, err
			}
			defer stmt.Close()
		} else {
			stmt, err = tx.Prepare("UPDATE proc SET name = $1, orig = $2, ts = $3 WHERE id = $4;")
			if err != nil {
				log.Error(err)
				return process, eventSource, eventStreams, err
			}
			if _, err = stmt.Exec(
				process.Name,
				process.Origin,
				eventSource.UpdatedAt,
				process.Id); err != nil {
				log.Error(err)
				tx.Rollback()
				return process, eventSource, eventStreams, err
			}
			defer stmt.Close()
		}
	}
	//record subsprocess
	{
		stmt, err = tx.Prepare("INSERT INTO sproc VALUES ($1, $2, $3, $4, $5);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		if _, err = stmt.Exec(
			process.SubProcess.Id,
			process.Id,
			process.SubProcess.Name,
			process.Origin,
			eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		if err = tx.QueryRow("SELECT ver FROM es WHERE id = $1", eventSource.ID).
			Scan(&lastVersion); err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if lastVersion != eventSource.Version {
			err = errors.New("concurrency exception : version does not match with command layer version")
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		stmt, err = tx.Prepare("UPDATE es SET ver = $1, ts_m = $2 WHERE id = $3 RETURNING *;")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		if err = stmt.QueryRow(eventSource.Version+1, eventSource.UpdatedAt, eventSource.ID).
			Scan(
				&eventSource.ID,
				&eventSource.Name,
				&eventSource.Version,
				&eventSource.CreatedAt,
				&eventSource.UpdatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return process, eventSource, eventStreams, err
		}
		defer stmt.Close()
	}
	{
		stmt, err = tx.Prepare("INSERT INTO e VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")
		if err != nil {
			log.Error(err)
			return process, eventSource, eventStreams, err
		}

		for _, e := range eventStreams {
			if _, err = stmt.Exec(
				process.Id,
				eventSource.UpdatedAt,
				process.SubProcess.Id,
				eventSource.UpdatedAt,
				eventSource.ID,
				eventSource.Name,
				eventSource.Version,
				e.Sequence,
				e.Type,
				e.Data); err != nil {
				tx.Rollback()
				log.Error(err)
				return process, eventSource, eventStreams, err
			}
		}

		defer stmt.Close()
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return process, eventSource, eventStreams, err
	}

	return process, eventSource, eventStreams, err
}

func (c *C_DB) ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) ([]*es.Event, error) {
	var (
		tx     *sql.Tx
		stmt   *sql.Stmt
		events []*es.Event
		err    error
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return events, err
	}

	stmt, err = tx.Prepare("SELECT id, " +
		"proc_id, " +
		"proc_ts, " +
		"sproc_id, " +
		"es_id, " +
		"es_name, " +
		"ver, " +
		"seq, " +
		"type, " +
		"data " +
		"from e " +
		"WHERE es_id=$1 AND es_name=$2 AND ver BETWEEN $3 AND $4;")
	if err != nil {
		log.Error(err)
		return events, err
	}

	eventsFromDB, _err := stmt.Query(eventSource.ID, eventSource.Name, minVersion, maxVersion)
	if _err != nil {
		log.Error(_err)
		return events, _err
	}
	defer eventsFromDB.Close()

	for eventsFromDB.Next() {
		event := es.Event{}
		err = eventsFromDB.Scan(
			&event.ID,
			&event.ProcessID,
			&event.ProcessCreatedAt,
			&event.SubProcessID,
			&event.EventSourceID,
			&event.EventSourceName,
			&event.Version,
			&event.Sequence,
			&event.Type,
			&event.Data,
			&event.CreatedAt,
		)
		if err != nil {
			log.Error(err)
			return events, err
		}
		events = append(events, &event)
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return events, err
	}

	return events, err
}

func (c *C_DB) WriteSnapshot(snapshot *es.Snapshot) (*pb.Status, error) {
	var (
		tx       *sql.Tx
		stmt     *sql.Stmt
		err      error
		status   *pb.Status
		tempData []byte
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return status, err
	}

	snapshotExists := 0
	if err = tx.QueryRow("SELECT COUNT(*) FROM ss WHERE es_id = $1 AND es_name = $2 AND ver = $3",
		snapshot.EventSourceID,
		snapshot.EventSourceName,
		snapshot.Version).
		Scan(&snapshotExists); err != nil {
		log.Error(err)
		return status, err
	}

	if snapshotExists > 0 {
		//Update used when changing values in the same version
		stmt, err = tx.Prepare("UPDATE ss SET ver = $1, data = $2, ts = $3 WHERE es_id = $4 AND es_name = $5;")
		if err != nil {
			log.Error(err)
			return status, err
		}

		if _, err = stmt.Exec(
			snapshot.Version,
			snapshot.Data,
			snapshot.CreatedAt,
			snapshot.EventSourceID,
			snapshot.EventSourceName); err != nil {
			log.Error(err)
			tx.Rollback()
			return status, err
		}

		defer stmt.Close()
	} else {
		stmt, err = tx.Prepare("INSERT INTO ss VALUES ($1, $2, $3, $4, $5);")
		if err != nil {
			log.Error(err)
			return status, err
		}

		if _, err = stmt.Exec(
			snapshot.EventSourceName,
			snapshot.EventSourceID,
			snapshot.Version,
			snapshot.Data,
			snapshot.CreatedAt); err != nil {
			tx.Rollback()
			log.Error(err)
			return status, err
		}

		defer stmt.Close()
	}

	if err = tx.QueryRow("SELECT data FROM ss WHERE es_id = $1 AND es_name = $2 AND ver = $3",
		snapshot.EventSourceID,
		snapshot.EventSourceName,
		snapshot.Version).
		Scan(&tempData); err != nil {
		log.Error(err)
		return status, err
	}

	err = json.Unmarshal(tempData, &status)
	if err != nil {
		log.Error(err)
		return status, err
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return status, err
	}

	return status, err
}

func (c *C_DB) ReadSnapshot(snapshot *es.Snapshot) (*pb.Status, error) {
	var (
		err    error
		status *pb.Status
		data   []byte
	)

	if err = c.db.QueryRow(`SELECT data FROM ss WHERE es_id = $1
		AND es_name = $2
		AND ver = $3`,
		snapshot.EventSourceID,
		snapshot.EventSourceName,
		snapshot.Version).Scan(&data); err != nil {
		log.Error(err)
		return status, err
	}

	err = json.Unmarshal(data, &status)
	if err != nil {
		log.Error(err)
		return status, err
	}

	return status, err
}

func (c *C_DB) ReadLatestSnapshot(eventSource es.EventSource) (*pb.Status, error) {
	var (
		err    error
		tx     *sql.Tx
		stmt   *sql.Stmt
		status *pb.Status
	)

	tx, err = c.db.Begin()
	if err != nil {
		tx.Rollback()
		log.Error(err)
		return status, err
	}

	stmt, err = tx.Prepare("SELECT data FROM ss WHERE es_id = $1 AND es_name = $2;")
	if err != nil {
		log.Error(err)
		return status, err
	}

	data, _err := stmt.Query(eventSource.ID, eventSource.Name)
	if _err != nil {
		log.Error(err)
		return status, _err
	}
	defer data.Close()

	var tempData []byte
	tempStatus := &pb.Status{}
	for data.Next() {
		err = data.Scan(&tempData)
		if err != nil {
			log.Error(err)
			return status, err
		}

		err = json.Unmarshal(tempData, &tempStatus)
		if err != nil {
			log.Error(err)
			return status, err
		}
	}

	if err = tx.Commit(); err != nil {
		log.Error(err)
		return status, err
	}

	status = tempStatus

	return status, err
}
