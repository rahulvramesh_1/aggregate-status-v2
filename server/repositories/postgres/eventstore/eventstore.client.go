package eventstore

import (
	"database/sql"
	"fmt"

	"bitbucket.org/evhivetech/aggregate-status-v2/server/common"
	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
)

var pgsess *sql.DB

func Connect() *sql.DB {
	var err error

	pgsess, err = sql.Open("postgres", fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		common.AppConfig.Eventstore.Host,
		common.AppConfig.Eventstore.Port,
		common.AppConfig.Eventstore.Username,
		common.AppConfig.Eventstore.Password,
		common.AppConfig.Eventstore.DB,
		common.AppConfig.Eventstore.SSLMode))

	if err != nil {
		log.Errorf("EVENTSTORE : failed to connect to %s (PGSQL)", common.AppConfig.Eventstore.DB)
	} else {
		log.Infof("EVENTSTORE : connected to %s (PGSQL)", common.AppConfig.Eventstore.DB)
	}

	return pgsess
}
