// package etl
//
// import (
// 	"database/sql"
//
// 	log "github.com/sirupsen/logrus"
// )
//
// func Init(db *sql.DB) {
// 	var (
// 		err error
// 	)
// 	_, err = db.Exec(`
// 		CREATE TABLE IF NOT EXISTS locations (
// 			id SERIAL PRIMARY KEY NOT NULL,
// 			ver INT NOT NULL,
// 			name TEXT NOT NULL,
// 			description TEXT,
// 			slug VARCHAR(128) NOT NULL,
// 			address TEXT NOT NULL,
// 			gallery_id INT NOT NULL,
// 			city VARCHAR(48) NOT NULL,
// 			state VARCHAR(48) NOT NULL,
// 			zipcode VARCHAR(16),
// 			country VARCHAR(32) NOT NULL,
// 			unique_code VARCHAR(16) NOT NULL UNIQUE,
// 			timezone VARCHAR(16) NOT NULL,
// 			notification_email VARCHAR(32) NOT NULL,
// 			reply_to_email VARCHAR(32) NOT NULL,
// 			from_email VARCHAR(32) NOT NULL,
// 			status_id INT NOT NULL,
// 			created_by INT NOT NULL,
// 			lat DOUBLE PRECISION,
// 			lon DOUBLE PRECISION,
// 			created_at TIMESTAMP WITH TIME ZONE NOT NULL,
// 			updated_at TIMESTAMP WITH TIME ZONE
// 		);
//
// 		CREATE INDEX IF NOT EXISTS location_ts_idx ON locations (timezone('UTC'::text, created_at), unique_code, city, state, country);
// 		`)
// 	if err != nil {
// 		log.Error(err)
// 	}
// }
