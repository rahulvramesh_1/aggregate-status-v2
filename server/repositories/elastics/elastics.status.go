package elastics

import (
	"context"
	"errors"
	"strconv"
	"strings"

	json "github.com/json-iterator/go"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
	"bitbucket.org/evhivetech/aggregate-status-v2/server/repositories"
	"github.com/labstack/gommon/log"
	"github.com/olivere/elastic"
)

type Q_DB struct {
	db *elastic.Client
}

func GetKeyValue(status *pb.Status, key string) (string, error) {
	var keyValue string
	var err error

	switch key {
	case "id":
		keyValue = strconv.Itoa(int(status.Id))
	case "name":
		keyValue = strings.ToLower(status.Name)
	case "description":
		keyValue = strings.ToLower(status.Description)
	default:
		err = errors.New("Wrong key")
	}

	return keyValue, err
}

func GetQueryClientDB(db *elastic.Client) repository.Q {
	return &Q_DB{db: db}
}

func (q *Q_DB) AddOne(status *pb.Status) (*pb.Status, error) {
	var err error

	_, err = q.db.Index().
		Index("status").
		Type("doc").
		Id(strconv.Itoa(int(status.Id))).
		BodyJson(status).Refresh("wait_for").Do(context.Background())

	if err != nil {
		log.Error(err)
		return status, err
	}

	return status, err
}

func (q *Q_DB) FindOne(status *pb.Status, key string) (*pb.Status, error) {
	var (
		_status  *pb.Status
		keyValue string
		_err     error
	)

	keyValue, _err = GetKeyValue(status, key)
	if _err != nil {
		log.Error(_err)
		return _status, _err
	}

	termQuery := elastic.NewTermQuery(key, keyValue)
	result, err := q.db.Search().
		Index("status").
		Query(termQuery).
		From(0).Size(1).
		Do(context.Background())

	if err != nil {
		log.Error(err)
		return _status, err
	}

	if result.Hits.TotalHits > 0 {
		for _, hit := range result.Hits.Hits {
			err := json.Unmarshal(*hit.Source, &_status)
			if err != nil {
				log.Error(err)
				return _status, err
			}
		}
	}

	return _status, err
}

func (q *Q_DB) FindAll(status *pb.SearchStatus, limit int, offset int, key string) ([]*pb.Status, error) {
	var (
		keyValues []string
		err       error
	)

	listStatus := []*pb.Status{}

	a := 0
	for len(listStatus) < limit {
		_status := &pb.Status{}

		switch key {
		case "id":
			if len(status.Id) != 0 && a < len(status.Id) {
				_status.Id = int64(status.Id[a])
			} else {
				return listStatus, err
			}
		case "name":
			if len(status.Name) != 0 && a < len(status.Name) {
				_status.Name = status.Name[a]
			} else {
				return listStatus, err
			}
		case "description":
			if len(status.Description) != 0 && a < len(status.Description) {
				_status.Description = status.Description[a]
			} else {
				return listStatus, err
			}
		default:
			err := errors.New("Wrong key")
			return listStatus, err
		}

		keyValue, _err := GetKeyValue(_status, key)
		if _err != nil {
			log.Error(_err)
			return listStatus, _err
		}

		keyValues = append(keyValues, keyValue)

		termQuery := elastic.NewTermQuery(key, keyValues[a])
		result, err := q.db.Search().
			Index("status").
			Query(termQuery).
			From(offset).Size(limit).
			Do(context.Background())

		if err != nil {
			log.Error(err)
			return listStatus, err
		}

		if result.Hits.TotalHits > 0 {
			i := 0
			for _, hit := range result.Hits.Hits {
				tempStatus := &pb.Status{}
				listStatus = append(listStatus, tempStatus)
				err := json.Unmarshal(*hit.Source, &listStatus[i])
				if err != nil {
					log.Error(err)
					return listStatus, err
				}
				i += 1
			}
		}
		a += 1
	}

	return listStatus, err
}

func (q *Q_DB) UpdateOne(status *pb.Status) (*pb.Status, error) {
	var err error
	_, err = q.db.Index().
		Index("status").
		Type("doc").
		Id(strconv.Itoa(int(status.Id))).
		BodyJson(status).
		Do(context.Background())
	if err != nil {
		log.Error(err)
		return status, err
	}
	return status, err
}
