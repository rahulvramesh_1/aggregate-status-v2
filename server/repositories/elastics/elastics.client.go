package elastics

import (
	"fmt"

	"bitbucket.org/evhivetech/aggregate-status-v2/server/common"
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"
)

var elsess *elastic.Client

func Connect() *elastic.Client {
	var err error
	elsess, err = elastic.NewClient(
		elastic.SetSniff(common.AppConfig.Elastic.Sniff),
		elastic.SetHealthcheck(common.AppConfig.Elastic.HealthCheck),
		elastic.SetURL(fmt.Sprintf("%s://%s:%d", common.AppConfig.Elastic.Scheme, common.AppConfig.Elastic.Host, common.AppConfig.Elastic.Port)),
		elastic.SetBasicAuth(common.AppConfig.Elastic.Username, common.AppConfig.Elastic.Password),
		elastic.SetScheme(common.AppConfig.Elastic.Scheme),
	)

	if err != nil {
		log.Errorf("QUERY : failed to connect to %s (ES)", err)
	} else {
		log.Infof("QUERY : connected to %s (ES)", common.AppConfig.Elastic.Host)
	}

	return elsess
}
