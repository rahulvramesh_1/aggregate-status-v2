package repository

import (
	es "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
)

//Using Command Query segragation
type Q interface {
	FindOne(status *pb.Status, key string) (*pb.Status, error)
	FindAll(status *pb.SearchStatus, limit int, offset int, key string) ([]*pb.Status, error)
	AddOne(status *pb.Status) (*pb.Status, error)
	UpdateOne(status *pb.Status) (*pb.Status, error)
}

type C interface {
	WriteNewStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	AppendToStream(proc *pb.Process, eventSource es.EventSource, eventStreams []es.Event) (*pb.Process, es.EventSource, []es.Event, error)
	ReadStream(eventSource es.EventSource, minVersion int, maxVersion int) ([]*es.Event, error)

	WriteSnapshot(snapshot *es.Snapshot) (*pb.Status, error)
	ReadSnapshot(snapshot *es.Snapshot) (*pb.Status, error)
	ReadLatestSnapshot(eventSource es.EventSource) (*pb.Status, error)
}
