package grpc_presenter

import (
	"encoding/json"
	"errors"
	"math"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
	"bitbucket.org/evhivetech/aggregate-status-v2/server/usecases"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type StatusGRPCPresenter struct {
	usecase usecases.UsecaseStatus
}

func NewStatusGRPCPresenter(sv *grpc.Server, uc usecases.UsecaseStatus) {
	p := &StatusGRPCPresenter{
		usecase: uc,
	}

	pb.RegisterServiceStatusServer(sv, p)
}

func (p *StatusGRPCPresenter) FindAll(ctx *pb.RequestFindAllStatus, stream pb.ServiceStatus_FindAllServer) error {
	var (
		err      error
		response *pb.ResponseFindAllStatus
	)

	response = &pb.ResponseFindAllStatus{
		Pagination: &pb.ResponsePagination{
			Limit: ctx.Pagination.Limit,
		},
	}

	if ctx.Pagination.PageNumber == 0 {
		response.Pagination.PageNumber = 1
	} else {
		response.Pagination.PageNumber = ctx.Pagination.PageNumber
	}

	response.Status, err = p.usecase.FindAll(ctx.GetStatus(), int(ctx.Pagination.Limit), int(ctx.Pagination.Offset), ctx.Key)
	if err != nil {
		log.Error(err)
		return err
	}

	if len(response.Status) < 10 {
		response.Pagination.TotalPage = 1
		response.Pagination.TotalItems = int32(len(response.Status))
	} else {
		responseStatusLengthAsFloat64 := float64(len(response.Status))
		responseStatusLengthAsint32 := int32(len(response.Status))

		//10 is number of status in one page
		response.Pagination.TotalPage = responseStatusLengthAsint32 / 10
		if math.Mod(responseStatusLengthAsFloat64, 10) != 0 {
			response.Pagination.TotalPage += 1
		}

		response.Pagination.TotalItems = responseStatusLengthAsint32
	}

	for i := 0; i < len(response.Status); i++ {
		if err := stream.Send(response); err != nil {
			return err
		}
	}

	return err
}

func (p *StatusGRPCPresenter) FindOne(ctx context.Context, params *pb.RequestFindOneStatus) (*pb.ResponseFindOneStatus, error) {
	var (
		err      error
		response *pb.ResponseFindOneStatus
	)

	response = &pb.ResponseFindOneStatus{
		Status: &pb.Status{},
	}

	response.Status, err = p.usecase.FindOne(params.GetStatus(), params.Key)
	if err != nil {
		log.Error(err)
		return response, err
	}
	return response, err
}

func (p *StatusGRPCPresenter) Add(ctx context.Context, params *pb.RequestProcessStatus) (*pb.ResponseProcessStatus, error) {
	var (
		err      error
		response *pb.ResponseProcessStatus
	)

	response = &pb.ResponseProcessStatus{
		Process: params.GetProcess(),
		Status:  params.GetStatus(),
	}

	response.Process, response.Status, err = p.usecase.AddOne(params.GetProcess(), params.GetStatus())
	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *StatusGRPCPresenter) Update(ctx context.Context, params *pb.RequestProcessStatus) (*pb.ResponseProcessStatus, error) {
	var (
		err      error
		response *pb.ResponseProcessStatus
	)

	response = &pb.ResponseProcessStatus{
		Process: params.GetProcess(),
		Status:  params.GetStatus(),
	}

	response.Process, response.Status, err = p.usecase.UpdateOne(params.GetProcess(), params.GetStatus())

	if err != nil {
		log.Error(err)
		response.Process.SubProcess.Error = &pb.ErrorSubProcess{
			Code:    1,
			Message: err.Error(),
		}
		ResponseError, err := json.Marshal(&response)
		if err != nil {
			log.Error(err)
			return response, err
		}
		return response, errors.New(string(ResponseError))
	}

	return response, nil
}

func (p *StatusGRPCPresenter) Remove(ctx context.Context, params *pb.RequestProcessStatus) (*pb.ResponseProcessStatus, error) {
	var (
		err      error
		response *pb.ResponseProcessStatus
		proc     *pb.Process
		status   *pb.Status
	)

	proc, status, err = p.usecase.RemoveOne(params.GetProcess(), params.GetStatus())

	response = &pb.ResponseProcessStatus{
		Process: proc,
		Status:  status,
	}

	return response, err
}

func (p *StatusGRPCPresenter) Rollback(ctx context.Context, params *pb.RequestRollbackStatus) (*pb.ResponseRollbackStatus, error) {
	var (
		err      error
		response *pb.ResponseRollbackStatus
	)
	return response, err
}
