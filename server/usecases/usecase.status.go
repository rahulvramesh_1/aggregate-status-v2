package usecases

import (
	"encoding/json"
	"errors"
	"math"
	"time"

	es "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/events"
	pb "bitbucket.org/evhivetech/aggregate-status-v2/server/entities/status"
	"bitbucket.org/evhivetech/aggregate-status-v2/server/repositories"
	"github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
)

type UsecaseStatus interface {
	FindOne(params *pb.SearchStatus, key string) (*pb.Status, error)
	FindAll(params *pb.SearchStatus, limit int, offset int, key string) ([]*pb.Status, error)
	Count(params *pb.SearchStatus) (int, error)

	AddOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error)
	UpdateOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error)
	RemoveOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error)
}

type RepositoryStatus struct {
	C repository.C
	Q repository.Q
}

func NewStatusUseCase(c repository.C, q repository.Q) UsecaseStatus {
	return &RepositoryStatus{
		C: c,
		Q: q,
	}
}

func (repo *RepositoryStatus) FindOne(params *pb.SearchStatus, key string) (*pb.Status, error) {
	var (
		status *pb.Status
		err    error
	)
	_params := &pb.Status{}

	switch key {
	case "id":
		if len(params.Id) != 0 {
			_params.Id = int64(params.Id[0])
		}
	case "name":
		if len(params.Name) != 0 {
			_params.Name = params.Name[0]
		}
	case "description":
		if len(params.Description) != 0 {
			_params.Description = params.Description[0]
		}
	default:
		err = errors.New("Wrong key")
	}
	if err != nil {
		log.Error(err)
		return status, err
	}

	status, err = repo.Q.FindOne(_params, key)
	if err != nil {
		log.Error(err)
		return status, err
	}

	return status, err
}

func (repo *RepositoryStatus) FindAll(params *pb.SearchStatus, limit int, offset int, key string) ([]*pb.Status, error) {
	var (
		status []*pb.Status
		err    error
	)

	status, err = repo.Q.FindAll(params, limit, offset, key)
	if err != nil {
		log.Error(err)
		return status, err
	}

	return status, err
}

func (repo *RepositoryStatus) Count(params *pb.SearchStatus) (int, error) {
	var (
		totalItems int
		err        error
	)
	return totalItems, err
}

func (repo *RepositoryStatus) AddOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error) {
	var (
		err    error
		procTs time.Time
	)

	//build events from status
	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, status, err
	}
	prev := &pb.Status{}
	eventSource := es.EventSource{
		ID:        0,
		Name:      es.ENTITY_NAME,
		Version:   0,
		CreatedAt: procTs,
		UpdatedAt: time.Time{},
	}

	eventStreams, err := es.BuildEventsFromStatus(proc, prev, status)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.WriteNewStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//write snapshot
	tempStatus := &pb.Status{}
	status, err = es.BuildStatusFromEvents(eventSource, eventStreams, tempStatus)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	data, _ := json.Marshal(&status)
	ss := &es.Snapshot{
		EventSourceName: eventSource.Name,
		EventSourceID:   eventSource.ID,
		Version:         eventSource.Version,
		Data:            string(data),
		CreatedAt:       eventSource.CreatedAt,
	}
	status, err = repo.C.WriteSnapshot(ss)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//create projection on query side
	status, err = repo.Q.AddOne(status)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	return proc, status, err
}

func (repo *RepositoryStatus) UpdateOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error) {
	var (
		err    error
		procTs time.Time
	)

	//sleep is to ensure the data in elasticsearch is the latest
	time.Sleep(1 * time.Second)

	//get version from status
	prev, err := repo.Q.FindOne(status, "id")
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	createdAt, err := ptypes.Timestamp(prev.CreatedAt)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	procTs, err = ptypes.Timestamp(proc.GetTimestamp())
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//check if this version does not match with current version
	if prev.Version != status.Version {
		err = errors.New("concurrency exception : version does not match with query layer version")
		log.Error(err)
		return proc, status, err
	}

	//fill in created at from previous record and updated at using process timestamp
	eventSource := es.EventSource{
		ID:        status.Id,
		Name:      es.ENTITY_NAME,
		Version:   status.Version,
		CreatedAt: createdAt,
		UpdatedAt: procTs,
	}

	eventStreams, err := es.BuildEventsFromStatus(proc, prev, status)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//write as new stream
	proc, eventSource, eventStreams, err = repo.C.AppendToStream(proc, eventSource, eventStreams)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	//build status
	status, err = es.BuildStatusFromEvents(eventSource, eventStreams, prev)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	if math.Mod(float64(eventSource.Version), 5) == 0 {
		data, _ := json.Marshal(&status)
		ss := &es.Snapshot{
			EventSourceName: eventSource.Name,
			EventSourceID:   eventSource.ID,
			Version:         eventSource.Version,
			Data:            string(data),
			CreatedAt:       eventSource.CreatedAt,
		}
		status, err = repo.C.WriteSnapshot(ss)
		if err != nil {
			log.Error(err)
			return proc, status, err
		}
	}

	//create projection on query side
	status, err = repo.Q.UpdateOne(status)
	if err != nil {
		log.Error(err)
		return proc, status, err
	}

	return proc, status, err
}

func (repo *RepositoryStatus) RemoveOne(proc *pb.Process, status *pb.Status) (*pb.Process, *pb.Status, error) {
	var (
		err error
	)
	return proc, status, err
}
