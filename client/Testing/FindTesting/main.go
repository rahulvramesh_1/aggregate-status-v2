package main

import (
	"context"
	"fmt"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/client/entities/status"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceStatusClient(conn)
	//timestamp, _ := ptypes.TimestampProto(time.Now())

	/*
			dummy data
			ts := time.Now().UnixNano() / int64(time.Millisecond)
			fmt.Printf("\nsent at : %d", ts)

				timestamp, test := ptypes.TimestampProto(time.Now())

		if test != nil {
			fmt.Print(test)
		}

			response, err := c.Add(context.Background(), &pb.RequestProcessStatus{
				Process: &pb.Process{
					Id:            "cdbef81e-84c0-11e8-adc0-fa7ae01bbebc",
					Name:          "Process B Create",
					Origin:        "Coordinator B",
					Timestamp:     timestamp,
					TotalSequence: 5,
					UserId:        1,
					SubProcess: &pb.SubProcess{
						Id:         "cdbef94a-84c0-11e8-adc0-fa7ae01bbebc",
						Name:       pb.SubProcessName_CREATE_STATUS,
						Timestamp:  timestamp,
						SequenceOf: 1,
					},
				},
				Status: &pb.Status{
					Id:          1,
					Name:        "Active",
					Description: "For active state",
					CreatedAt:   timestamp,
					UpdatedAt:   timestamp,
					Version:     1,
				},
			})

			if err != nil {
				log.Fatalf("Error when calling add : %s", err)
			} else {
				log.Infof("Add data success")
				fmt.Printf("\n %s", response.GetProcess())
				fmt.Printf("\n %s", response.GetStatus())
			}
	*/

	requestFindOneStatus := pb.RequestFindOneStatus{
		Status: &pb.SearchStatus{},
	}
	//requestFindOneStatus.Status.Id = append(requestFindOneStatus.Status.Id, 2)
	requestFindOneStatus.Status.Name = append(requestFindOneStatus.Status.Name, "Active")
	//requestFindOneStatus.Status.Description = append(requestFindOneStatus.Status.Description, "For active state")
	//requestFindOneStatus.Status.CreatedAt = append(requestFindOneStatus.Status.CreatedAt, timestamp)
	//requestFindOneStatus.Status.UpdatedAt = append(requestFindOneStatus.Status.UpdatedAt, timestamp)
	requestFindOneStatus.Key = "name"

	request, err := c.FindOne(context.Background(), &requestFindOneStatus)

	if err != nil {
		log.Fatalf("Error when finding status : %s", err)
	} else {
		log.Infof("Get data from key name")
		fmt.Printf("\n %s", request.GetStatus())
	}
}
