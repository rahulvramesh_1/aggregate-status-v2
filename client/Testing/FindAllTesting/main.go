package main

import (
	"fmt"
	"io"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/client/entities/status"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn *grpc.ClientConn
	)

	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceStatusClient(conn)

	requestFindAllStatus := pb.RequestFindAllStatus{
		Status: &pb.SearchStatus{},
		Pagination: &pb.RequestPagination{
			Limit:      24,
			PageNumber: 1,
			Offset:     0,
		},
		Key: "name",
	}
	//requestFindAllStatus.Status.Id = append(requestFindAllStatus.Status.Id, 1)
	requestFindAllStatus.Status.Name = append(requestFindAllStatus.Status.Name, "Updated")
	//requestFindAllStatus.Status.Description = append(requestFindAllStatus.Status.Description, "Updated description")

	stream, err := c.FindAll(context.Background(), &requestFindAllStatus)

	if err != nil {
		log.Fatalf("Error when finding status : %s", err)
	} else {
		var responsePagination pb.ResponsePagination
		i := 0
		fmt.Println("Data from stream: ")
		for {
			dataFromServer, err := stream.Recv()
			if i == 0 {
				responsePagination.TotalPage = dataFromServer.Pagination.TotalPage
				responsePagination.TotalItems = dataFromServer.Pagination.TotalItems
				responsePagination.Limit = dataFromServer.Pagination.Limit
				responsePagination.PageNumber = dataFromServer.Pagination.PageNumber
			}

			if err == io.EOF {
				break
			}

			if err != nil {
				log.Println("Error", err)
				break
			}

			fmt.Printf("Id: %d,  Name: %s, Description: %s, CreatedAt: %v, UpdatedAt: %v, Version: %d\n",
				dataFromServer.Status[i].Id,
				dataFromServer.Status[i].Name,
				dataFromServer.Status[i].Description,
				dataFromServer.Status[i].CreatedAt,
				dataFromServer.Status[i].UpdatedAt,
				dataFromServer.Status[i].Version,
			)
			i += 1
		}
		fmt.Println("Pagination")
		fmt.Printf("Total Page: %d, Total Items: %d, Limit: %d, Page Number: %d",
			responsePagination.TotalPage,
			responsePagination.TotalItems,
			responsePagination.Limit,
			responsePagination.PageNumber)
	}
}
