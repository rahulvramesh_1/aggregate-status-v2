package main

import (
	"fmt"
	"strconv"
	"time"

	pb "bitbucket.org/evhivetech/aggregate-status-v2/client/entities/status"
	ptypes "github.com/golang/protobuf/ptypes"
	log "github.com/sirupsen/logrus"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	var (
		conn               *grpc.ClientConn
		responseTimeCreate int64
		responseTimeUpdate int64
		version            int64
	)

	totalLoop := int64(10)
	conn, err := grpc.Dial(":5000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()
	c := pb.NewServiceStatusClient(conn)

	ts := time.Now().UnixNano() / int64(time.Millisecond)
	fmt.Printf("\nsent at : %d", ts)

	timestamp, _ := ptypes.TimestampProto(time.Now())

	response, err := c.Add(context.Background(), &pb.RequestProcessStatus{
		Process: &pb.Process{
			Id:            "1efe227e-7f4c-11e8-adc0-fa7ae01bbebc",
			Name:          "Process A Create",
			Origin:        "Coordinator A",
			Timestamp:     timestamp,
			TotalSequence: 5,
			UserId:        1,
			SubProcess: &pb.SubProcess{
				Id:         "1efe24fe-7f4c-11e8-adc0-fa7ae01bbebc",
				Name:       pb.SubProcessName_CREATE_STATUS,
				Timestamp:  timestamp,
				SequenceOf: 1,
			},
		},
		Status: &pb.Status{
			Id:          1,
			Name:        "New status",
			Description: "New description",
			CreatedAt:   timestamp,
			UpdatedAt:   timestamp,
			Version:     1,
		},
	})

	if err != nil {
		log.Fatalf("Error when calling add : %s", err)
	}

	receivedAt := time.Now().UnixNano() / int64(time.Millisecond)
	responseTime := (receivedAt - ts)
	responseTimeCreate += responseTime
	fmt.Printf("\nreceived at : %d, response time : %d", receivedAt, responseTime)
	fmt.Printf("\n Response from server: \n Process=%s \n status=%s \n\n\n", response.GetProcess(), response.GetStatus())

	version = response.GetStatus().Version
	for i := int64(0); i < totalLoop; i++ {
		ts = time.Now().UnixNano() / int64(time.Millisecond)
		fmt.Printf("\nsent at : %d", ts)
		timestamp, _ = ptypes.TimestampProto(time.Now())
		response, err := c.Update(context.Background(), &pb.RequestProcessStatus{
			Process: &pb.Process{
				Id:            "1efe227e-7f4c-11e8-adc0-fa7ae01bbebc",
				Name:          "Process A Update",
				Origin:        "Coordinator A",
				Timestamp:     timestamp,
				TotalSequence: 5,
				UserId:        1,
				SubProcess: &pb.SubProcess{
					Id:         "1efe24fe-7f4c-11e8-adc0-fa7ae01bbebc",
					Name:       pb.SubProcessName_UPDATE_STATUS,
					Timestamp:  timestamp,
					SequenceOf: 1,
				},
			},
			Status: &pb.Status{
				Id:          response.GetStatus().Id,
				Name:        response.GetStatus().Name + strconv.Itoa(int(i)),
				Description: response.GetStatus().Description + strconv.Itoa(int(i)),
				CreatedAt:   response.GetStatus().CreatedAt,
				UpdatedAt:   response.GetStatus().UpdatedAt,
				Version:     version,
			},
		})
		version = response.GetStatus().Version
		if err != nil {
			log.Fatalf("Error when calling update : %s", err)
		}

		receivedAt = time.Now().UnixNano() / int64(time.Millisecond)
		responseTime = (receivedAt - ts)
		responseTimeUpdate += responseTime
		fmt.Printf("\nreceived at : %d response time : %d", receivedAt, responseTime)
		fmt.Printf("\n Response from server: \n Process=%s \n status=%s", response.GetProcess(), response.GetStatus())
	}

	fmt.Printf("average response time create : %d", responseTimeCreate/totalLoop)
	fmt.Printf("average tme update : %d", responseTimeUpdate/totalLoop)
}
